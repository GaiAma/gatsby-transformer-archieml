"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "onCreateNode", {
  enumerable: true,
  get: function get() {
    return _onCreateNode.default;
  }
});

var _onCreateNode = _interopRequireDefault(require("./on-create-node"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }